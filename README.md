Comp 371 Project - RayTracer
----------------------------

School Project for: COMP 371 | Computer Graphics

The project was built with Visual Studio.

Make sure you set the following project properties:
Configuration Properties > C/C++ > General > Additional Include Directories = $(ProjectDir)include

Where to place scene files:
In the root directory, place scene files in '_scenes/' directory

Where to find the output:
The generated images will be located in the project root directory in a directory named '_output/'