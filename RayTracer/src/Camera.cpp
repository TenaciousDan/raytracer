#include "Camera.h"

#include "utilities/MathUtil.h"

Camera::Camera(const glm::vec3 position, const glm::vec3 up_vec, const glm::vec3 direction, float aspect_ratio, float focal_length, float zFar, float fov):
logger("Camera")
{
	this->position = position;
	this->up_vec = up_vec;
	this->direction = glm::normalize(direction);
	this->aspect_ratio = aspect_ratio;
	this->focal_length = focal_length;
	this->zFar = zFar;
	this->fov = fov;

	this->image_height = 2 * (glm::tan(glm::radians(fov) / 2) * focal_length);
	this->image_width = aspect_ratio * this->image_height;
	logger.debug("image size: (" + std::to_string(image_width) + ", " + std::to_string(image_height) + ")");

	this->cam_to_world_matrix = MathUtil::lookAt(this->position, this->position + this->direction, this->up_vec);
}

Camera::~Camera()
{
}

void Camera::imageToCameraSpace(glm::vec3& image_coord) const
{
	// center pixel and go from raster space -> NDC space:
	image_coord.x = (image_coord.x + 0.5f) / image_width;
	image_coord.y = (image_coord.y + 0.5f) / image_height;
	
	// NDC(Normalized Device Coordinates) space -> screen space:
	image_coord.x = (2 * image_coord.x) - 1;
	image_coord.y = 1 - (2 * image_coord.y);
	
	// screen space -> camera space:
	// account for aspect_ratio
	if(image_width > image_height)
		image_coord.x *= (image_width / image_height);
	else if(image_width < image_height)
		image_coord.y *= (image_height / image_width);
	
	// account for fov
	image_coord.x *= glm::tan(glm::radians(fov) / 2);
	image_coord.y *= glm::tan(glm::radians(fov) / 2);

	// apply focal_length
	image_coord.z = glm::clamp(focal_length * direction.z , -1.0f, 1.0f);
}

Ray Camera::generatePrimaryRay(float image_x, float image_y) const
{
	// raster space -> camera space
	glm::vec3 coordinates(image_x, image_y, this->direction.z);
	this->imageToCameraSpace(coordinates);

	// calculate ray_direction
	glm::vec3 ray_direction = glm::vec3(this->cam_to_world_matrix * glm::vec4(coordinates, 1.0f)) - position;
	glm::vec3 ray_origin = position + (direction * glm::vec3(MathUtil::EPSILON_BIAS));
	return Ray(Ray::Type::PRIMARY, ray_origin, ray_direction);
}
