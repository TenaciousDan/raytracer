#include "Renderer.h"
#include "utilities/MathUtil.h"

const std::string Renderer::OUTPUT_FILE_DIRECTORY("_output/");
const unsigned int Renderer::SAMPLE_COUNT = 25;

Renderer::Renderer(int sample_count)
{
	this->sample_count = sample_count;
}

Renderer::~Renderer()
{
}

void Renderer::render(Scene& scene, cimg_library::CImg<float>& image)
{
	// fire rays through each pixel in the image plane
	for (int y = 0; y < image.height(); y++)
	{
		for (int x = 0; x < image.width(); x++)
		{
			// collect mutiple samples of colors for the pixel (anti-aliasing)
			std::vector<glm::vec4> color_samples;
			for (int i = 0; i < sample_count; i++)
			{
				// offset the ray direction by a random amount [0,1] to get different samples (jittering)
				float pixel_x = (float)x + static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
				float pixel_y = (float)y + static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

				Ray ray = scene.getCamera().generatePrimaryRay(pixel_x, pixel_y);
				color_samples.push_back(this->fireRay(ray, scene, INFINITY));

			}

			// get the avg color of the samples
			glm::vec4 samples_sum(0, 0, 0, 0);
			for (unsigned int i = 0; i < color_samples.size(); i++)
				samples_sum += color_samples.at(i);

			glm::vec4 color(0, 0, 0, 0);
			color[0] = samples_sum[0] / sample_count;
			color[1] = samples_sum[1] / sample_count;
			color[2] = samples_sum[2] / sample_count;
			color[3] = samples_sum[3] / sample_count;

			// color pixel
			for (auto i = 0; i < image.spectrum(); i++)
				image(x, y, i) = i >= 3 ? 255.0f : color[i] * 255.0f;
		}
	}

	// watermark
	unsigned char purple[] = { 255, 0, 255 };
	image.draw_text(image.width() - 100, image.height() - 40, "~ TenaciousDan", purple);
}

glm::vec4 Renderer::fireRay(Ray & ray, Scene & scene, float max_depth)
{
	glm::vec4 final_color = scene.getBackgroundColor();
	
	// check for intersections
	float closest_distance = max_depth;
	int closest = -1;
	for (auto i = 0; i < scene.getObjects().size(); i++)
	{
		float t = -1.0f;
		if (scene.getObjects()[i]->intersects(ray, t) && t >= 0 && t < closest_distance)
		{
			closest_distance = t;
			closest = i;
		}
	}

	// we hit something
	if (closest >= 0)
		return this->computeColorAt(ray, scene, closest, closest_distance);

	return final_color;
}

glm::vec4 Renderer::computeColorAt(Ray & ray, Scene & scene, const int object_index, float distance)
{
	glm::vec4 color = scene.getBackgroundColor();
	glm::vec3 point_intersection = ray.getPoint(distance);
	glm::vec3 N = scene.getObjects()[object_index]->computeNormal(point_intersection);

	// light and shadow calculation
	glm::vec3 ambient = glm::vec3(scene.getObjects()[object_index]->getAmbientColor());
	glm::vec3 diffuse(0.0f);
	glm::vec3 specular(0.0f);
	for (Light* light : scene.getLights())
	{
		glm::vec3 I = glm::normalize(light->getPosition() - point_intersection);
		float cos_theta = glm::clamp(glm::dot(I, N), 0.0f, 1.0f);
		
		if (cos_theta > 0) // eliminate cases where I.N == 0 (causes shadow acne)
		{
			// check if point is in shadow
			bool in_shadow = false;
			float distance_to_light = glm::length(light->getPosition() - point_intersection);
			Ray shadow_ray(Ray::Type::SHADOW, point_intersection + (I * (MathUtil::EPSILON_BIAS)), I);

			for (Object* object : scene.getObjects())
			{
				float t = -1.0f;
				if (object->intersects(shadow_ray, t) && t < distance_to_light)
				{
					in_shadow = true;
					break;
				}
			}

			// calculate lighting if not in shadow
			if (!in_shadow)
			{
				// diffuse
				diffuse += cos_theta * light->getDiffuseColor() * glm::vec3(scene.getObjects()[object_index]->getDiffuseColor());

				// specular
				float shininess = scene.getObjects()[object_index]->getShininess();
				glm::vec3 V = glm::normalize(ray.getOrigin() - point_intersection);
				glm::vec3 R = glm::reflect(-I, scene.getObjects()[object_index]->computeNormal(point_intersection));
				specular += glm::pow(glm::clamp(glm::dot(V, R), 0.0f, 1.0f), shininess) * light->getSpecularColor() * glm::vec3(scene.getObjects()[object_index]->getSpecularColor());
			}
		}
	}

	color = glm::clamp(glm::vec4(ambient + diffuse + specular, 1), 0.0f, 1.0f);
	
	return color;
}