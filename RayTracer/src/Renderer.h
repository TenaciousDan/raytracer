#pragma once

#include <glm/ext.hpp>
#include <CImg/CImg.h>
#include <string>
#include "Logger.h"
#include "Scene.h"
#include "Ray.h"

class Renderer
{
private:
	Logger logger;
	int sample_count;

public:
	static const std::string OUTPUT_FILE_DIRECTORY;
	static const unsigned int SAMPLE_COUNT;

	Renderer(): Renderer(Renderer::SAMPLE_COUNT) {};
	Renderer(int sample_count);
	~Renderer();

	void render(Scene& scene, cimg_library::CImg<float>& image);
	glm::vec4 fireRay(Ray& ray, Scene& scene, float max_depth);
	glm::vec4 computeColorAt(Ray& ray, Scene& scene, const int object_index, float distance);
};