#include "Logger.h"
#include "App.h"
#include <ctime>

//// check for mem leaks in debug mode
// #define _CRTDBG_MAP_ALLOC
// #include <stdlib.h>
// #include <crtdbg.h>

int main(int argc, char** argv)
{
	 //// check for mem leaks in debug mode
	 //_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	srand((unsigned int)time(0));
	
	Logger logger("Main");
	logger.setLogLevel(ELogLevel::OFF);

	logger.debug("APPLICATION START...");

	App& app = App::getInstance();

	app.printAsciiArtTitle();
	app.start();

	logger.debug("APPLICATION END...");
	// system("pause");
	return EXIT_SUCCESS;
}