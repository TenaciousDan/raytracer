#pragma once

#include "Object.h"

class Plane : public Object
{
protected:
	glm::vec3 normal;
	glm::vec3 position;

public:
	Plane(Object::FaceCulling face_culling, glm::vec3 normal, glm::vec3 position): Plane(face_culling, Object::DEFAULT_COLOR, Object::DEFAULT_COLOR, Object::DEFAULT_COLOR, 32.0f, normal, position) {};
	Plane(Object::FaceCulling face_culling, glm::vec4 ambient_color, glm::vec4 diffuse_color, glm::vec4 specular_color, float shininess, glm::vec3 normal, glm::vec3 position);
	~Plane();

	virtual bool intersects(Ray& ray, float &t) const;

	virtual const glm::vec3 computeNormal(const glm::vec3& p) const { return normal; };

	virtual glm::vec3& getNormal() { return normal; };
	virtual glm::vec3& getPosition() { return position; };
};