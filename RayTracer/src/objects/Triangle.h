#pragma once

#include "Object.h"

class Triangle : public Object
{
protected:
	glm::vec3 p0;
	glm::vec3 p1;
	glm::vec3 p2;
	glm::vec3 normal;

public:
	Triangle(Object::FaceCulling face_culling, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2) : Triangle(face_culling, Object::DEFAULT_COLOR, Object::DEFAULT_COLOR, Object::DEFAULT_COLOR, 32.0f, p0, p1, p2) {};
	Triangle(Object::FaceCulling face_culling, glm::vec4 ambient_color, glm::vec4 diffuse_color, glm::vec4 specular_color, float shininess, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2);
	~Triangle();

	virtual bool intersects(Ray& ray, float &t) const;

	virtual const glm::vec3 computeNormal(const glm::vec3& p) const { return normal; };
	virtual glm::vec3& getNormal() { return normal; };

	virtual glm::vec3& getVertex(unsigned int vextex_num);
};