#pragma once

#include <glm/ext.hpp>
#include "../Ray.h"

class Object
{
public:
	static enum class FaceCulling { NONE, BACK, FRONT };

protected:
	Object::FaceCulling face_culling;
	glm::vec4 ambient_color;
	glm::vec4 diffuse_color;
	glm::vec4 specular_color;
	float shininess;

public:
	Object(): Object(Object::FaceCulling::BACK) {}
	Object(Object::FaceCulling face_culling): Object(face_culling, Object::DEFAULT_COLOR, Object::DEFAULT_COLOR, Object::DEFAULT_COLOR, 32.0f) {};
	Object(Object::FaceCulling face_culling, glm::vec4 ambient_color, glm::vec4 diffuse_color, glm::vec4 specular_color, float shininess);
	~Object();

	static const glm::vec4 DEFAULT_COLOR;

	virtual bool intersects(Ray& ray, float &t) const = 0;
	virtual const glm::vec3 computeNormal(const glm::vec3& p) const = 0;

	virtual glm::vec4& getAmbientColor() { return ambient_color; };
	virtual glm::vec4& getDiffuseColor() { return diffuse_color; };
	virtual glm::vec4& getSpecularColor() { return specular_color; };
	virtual float getShininess() { return shininess; };
};