#include "Sphere.h"

Sphere::Sphere(Object::FaceCulling face_culling, glm::vec4 ambient_color, glm::vec4 diffuse_color, glm::vec4 specular_color, float shininess, glm::vec3 center, float radius):
Object(face_culling, ambient_color, diffuse_color, specular_color, shininess)
{
	this->center = center;
	this->radius = radius;
}

Sphere::~Sphere()
{
}

/* Check if ray intersects with sphere. */
bool Sphere::intersects(Ray& ray, float & t) const
{
	/*
		ray-sphere intersection algorithm:
		---------------
		let d = ray_direction, o = sphere_origin, c = sphere_center, r = sphere_radius
		oc = o-c
		a = x_d^2 + y_d^2 + z_d^2 = 1 since |d| = 1
		b = 2(x_d(x_o - x_c) + y_d(y_o - y_c) + z_d(z_o - z_c)) = 2(x_d(x_oc) + y_d(y_oc) + z_d(z_oc)) = 2(d.(oc))
		c = (x_o - x_c)^2 + (y_o - y_c)^2 + (z_o - z_c)^2 - r^2 = ((oc).(oc)) - r^2
		disc = b^2 - 4ac
		abort if disc < 0
		t0 = (-b - sqrt(disc)) / 2a  <-- distance from ray_origin to the left side
		t1 = (-b + sqrt(disc)) / 2a  <-- distance from ray_origin to the right side
		t = min(t0, t1)
	*/

	glm::vec3 oc = ray.getOrigin() - this->center;
	float a = glm::dot(ray.getDirection(), ray.getDirection());
	float b = 2 * glm::dot(oc, ray.getDirection());
	float c = glm::dot(oc, oc) - (radius*radius); // c > 0 ? outside sphere : inside sphere
	
	// back face culling inside the sphere
	if (c < 0.0f && this->face_culling == Object::FaceCulling::BACK) return false;

	// front face culling outside the sphere
	if (c > 0.0f && this->face_culling == Object::FaceCulling::FRONT) return false;

	float discriminant = b * b - 4 * a*c;
	if (discriminant >= 0.0f)
	{
		discriminant = sqrt(discriminant);
		float t0 = -(b - discriminant) / 2*a;
		float t1 = -(b + discriminant) / 2*a;

		// pick the closer intersection if not negative
		t = (t0 < t1 && t0 >= 0.0f) ? t0 : t1;

		// front and 'no' face culling inside the sphere
		if (c < 0.0f && this->face_culling != Object::FaceCulling::BACK)
			t = t0;

		return (t >= 0.0f); // ignore anthing behind the ray
	}

	return false;
}

const glm::vec3 Sphere::computeNormal(const glm::vec3 & p) const
{
	return glm::normalize(p - center);
}
