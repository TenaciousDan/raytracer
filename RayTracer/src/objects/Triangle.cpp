#include "Triangle.h"

#include "../utilities/MathUtil.h"

Triangle::Triangle(Object::FaceCulling face_culling, glm::vec4 ambient_color, glm::vec4 diffuse_color, glm::vec4 specular_color, float shininess, glm::vec3 p0, glm::vec3 p1, glm::vec3 p2):
Object(face_culling, ambient_color, diffuse_color, specular_color, shininess)
{
	this->p0 = p0;
	this->p1 = p1;
	this->p2 = p2;
	this->normal = glm::normalize(glm::cross(p1 - p0, p2 - p0));
}

Triangle::~Triangle()
{
}

bool Triangle::intersects(Ray & ray, float & t) const
{
	/*
		Moller Trumbore Algorithm
		------------------------------
		This algorithm works by transforming the triangle from its original position in world space
		to the origin where the first vertex is located at the origin. We compute 3 vectors
		P, T and Q that represent the x, y and z axis in our new space. We then compute u and v which
		together are used to express coordinates (between 0 and 1) of points defined inside the projected
		triangle's PT space. We can then check to see if t which expresses a point on our T axis is within
		the triangle.
	*/

	// backface culling
	if (this->face_culling == Object::FaceCulling::BACK && glm::dot(ray.getDirection(), normal) > 0.0f) return false;

	// frontface culling
	if (this->face_culling == Object::FaceCulling::FRONT && glm::dot(ray.getDirection(), normal) < 0.0f) return false;

	glm::vec3 e0, e1;
	glm::vec3 P, Q, T;
	float u, v;

	e0 = p1 - p0;
	e1 = p2 - p0;
	P = glm::cross(ray.getDirection(), e1);

	float determinant = glm::dot(e0, P);

	// the ray and triangle are parallel
	if (determinant == 0.0f) return false;

	float inverse_determinant = 1.0f / determinant;

	T = ray.getOrigin() - p0;
	u = glm::dot(T, P) * inverse_determinant;
	if (u < 0.0f || u > 1.0f) return false; // we are outside the triangle

	Q = glm::cross(T, e0);
	v = glm::dot(ray.getDirection(), Q) * inverse_determinant;
	if (v < 0.0f || (u + v) > 1.0f) return false; // we are outside the triangle

	t = glm::dot(e1, Q) * inverse_determinant;

	if (t >= 0.0f) return true;

	return false;
}

glm::vec3& Triangle::getVertex(unsigned int vextex_num)
{
	if(vextex_num == 0) return p0;
	else if (vextex_num == 1) return p1;
	else if (vextex_num == 2) return p2;
	else return p0;
}
