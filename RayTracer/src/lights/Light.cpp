#include "Light.h"

Light::Light(glm::vec3 position, glm::vec3 diffuse_color, glm::vec3 specular_color)
{
	this->position = position;
	this->diffuse_color = diffuse_color;
	this->specular_color = specular_color;
}

Light::~Light()
{
}
