#pragma once

#include <iostream>
#include <string>

enum class ELogLevel
{
	ALL,
	DEBUG, INFO, WARN, ERR,
	OFF
};

class Logger
{
private:
	std::string context;
	static ELogLevel logLevel;

	std::string getLoggerPrefixString(std::string logLevel, std::string context);

public:
	Logger();
	Logger(std::string context);
	~Logger();

	static ELogLevel getLogLevel();

	static void setLogLevel(ELogLevel logLevel);

	void debug(std::string message);
	void debug(std::string context, std::string message);
	void info(std::string message);
	void info(std::string context, std::string message);
	void warn(std::string message);
	void warn(std::string context, std::string message);
	void err(std::string message);
	void err(std::string context, std::string message);
};