#include "Logger.h"

#include "utilities/CommonUtil.h"

ELogLevel Logger::logLevel = ELogLevel::ALL;

Logger::Logger() : Logger("") {}
Logger::Logger(std::string context)
{
	this->context = context;
}

Logger::~Logger()
{
}

ELogLevel Logger::getLogLevel()
{
	return Logger::logLevel;
}

void Logger::setLogLevel(ELogLevel logLevel)
{
	Logger::logLevel = logLevel;
}

std::string Logger::getLoggerPrefixString(std::string logLevel, std::string context)
{
	context = context == "" ? context : " <" + context + ">";
	return (CommonUtil::getDetailedCurrentTimeStamp() + " " + logLevel + context + " - ");
}

void Logger::debug(std::string message)
{
	this->debug(this->context, message);
}
void Logger::debug(std::string context, std::string message)
{
	if (Logger::logLevel <= ELogLevel::DEBUG)
	{
		std::cout << (this->getLoggerPrefixString("DEBUG", context) + message) << std::endl;
	}
}

void Logger::info(std::string message)
{
	this->info(this->context, message);
}
void Logger::info(std::string context, std::string message)
{
	if (Logger::logLevel <= ELogLevel::INFO)
	{
		std::cout << (this->getLoggerPrefixString("INFO", context) + message) << std::endl;
	}
}

void Logger::warn(std::string message)
{
	this->warn(this->context, message);
}
void Logger::warn(std::string context, std::string message)
{
	if (Logger::logLevel <= ELogLevel::WARN)
	{
		std::cout << (this->getLoggerPrefixString("WARN", context) + message) << std::endl;
	}
}

void Logger::err(std::string message)
{
	this->err(this->context, message);
}
void Logger::err(std::string context, std::string message)
{
	if (Logger::logLevel <= ELogLevel::ERR)
	{
		std::cerr << (this->getLoggerPrefixString("ERROR", context) + message) << std::endl;
	}
}