#include "Logger.h"
#include <string>
#include <iostream>

class App
{
public:
	static const unsigned int THREAD_COUNT = 1;

	class AppOptions
	{
	public:
		std::string scene_directory;
		std::string output_directory;
		unsigned int thread_count;
	};

private:
	App(); // singleton
	Logger logger;
	App::AppOptions options;

public:
	App(App const&) = delete; // singleton
	void operator=(App const&) = delete; // singleton
	~App();
	
	static App& getInstance();

	void start();
	void start(App::AppOptions options);
	void houseKeeping();

	// print fancy title to console :)
	static void printAsciiArtTitle();
};