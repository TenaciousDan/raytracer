#pragma once

#include <glm/ext.hpp>
#include <vector>
#include <string>
#include "Logger.h"
#include "Camera.h"
#include "lights/Light.h"
#include "objects/Object.h"

class Scene
{
private:
	Logger logger;
	glm::vec4 bg_color;
	Camera* camera;
	std::vector<Object*> objects;
	std::vector<Light*> lights;

	void createSceneFromFile(std::string filepath);

public:
	Scene(std::string filepath);
	Scene(glm::vec4 bg_color, Camera* cam, std::vector<Object*>& objects, std::vector<Light*>& lights);
	~Scene();

	static const glm::vec4 BG_COLOR;
	static const std::string SCENE_DIRECTORY;
	static const Object::FaceCulling FACE_CULLING;

	glm::vec4& getBackgroundColor() { return bg_color; };
	Camera& getCamera() { return *camera; };
	std::vector<Object*>& getObjects() { return objects; };
	std::vector<Light*>& getLights() { return lights; };
};